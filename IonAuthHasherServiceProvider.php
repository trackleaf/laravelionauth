<?php

use Illuminate\Support\ServiceProvider;

class IonAuthHasherServiceProvider extends ServiceProvider {

    public function register() {
        $this->app->bind('hash', function() {
            return new IonAuthHasher;
        });
    }

}
