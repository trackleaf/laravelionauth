LaravelIonAuth
==============

CodeIgniter Ion Auth Hashing for Laravel...to not break authentication when converting frameworks.

Based on Ben Edmunds Ion Auth:  https://github.com/benedmunds/CodeIgniter-Ion-Auth

1. replace 'Illuminate\Hashing\HashServiceProvider' with 'IonAuthHasherServiceProvider' in the providers array in app/config/app.php
2. add files to autoload classmap in composer.json. Example 'Acme/Hashing'
