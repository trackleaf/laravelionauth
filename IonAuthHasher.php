<?php

class IonAuthHasher implements Illuminate\Hashing\HasherInterface {

    private $rounds = 7;
    private $salt_prefix = "$2y$";
    private $randomState;

    public function make($value, array $options = array()) {
        $this->rounds = isset($options['rounds']) ? $options['rounds'] : 7;
        $this->salt_prefix = isset($options['salt_prefix']) ? $options['salt_prefix'] : "$2y$";

        $hash = crypt($value, $this->getSalt());
        if (strlen($hash) > 13) {
            return $hash;
        }

        return false;
    }

    public function check($value, $hashedValue, array $options = array()) {
        $this->rounds = isset($options['rounds']) ? $options['rounds'] : 7;
        $this->salt_prefix = isset($options['salt_prefix']) ? $options['salt_prefix'] : "$2y$";

        $hash = crypt($value, $hashedValue);
        return $hash === $hashedValue;
    }

    public function needsRehash($hashedValue, array $options = array()) {
        $this->rounds = isset($options['rounds']) ? $options['rounds'] : 7;
        $this->salt_prefix = isset($options['salt_prefix']) ? $options['salt_prefix'] : "$2y$";

        $cost = isset($options['rounds']) ? $options['rounds'] : $this->rounds;

        return password_needs_rehash($hashedValue, PASSWORD_BCRYPT, array('cost' => $cost));
    }

    private function getSalt() {
        //options should have rounds as well as salt prefix
        $salt = sprintf($this->salt_prefix . '%02d$', $this->rounds);
        $bytes = $this->getRandomBytes(17);
        $salt .= $this->encodeBytes($bytes);

        return $salt;
    }

    private function getRandomBytes($count) {
        $bytes = '';

        if (function_exists('openssl_random_pseudo_bytes') && (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')) { // OpenSSL slow on Win
            $bytes = openssl_random_pseudo_bytes($count);
        }

        if ($bytes === '' && @is_readable('/dev/urandom') && ($hRand = @fopen('/dev/urandom', 'rb')) !== FALSE) {
            $bytes = fread($hRand, $count);
            fclose($hRand);
        }

        if (strlen($bytes) < $count) {
            $bytes = '';

            if ($this->randomState === null) {
                $this->randomState = microtime();
                if (function_exists('getmypid')) {
                    $this->randomState .= getmypid();
                }
            }

            for ($i = 0; $i < $count; $i += 16) {
                $this->randomState = md5(microtime() . $this->randomState);
                if (PHP_VERSION >= '5') {
                    $bytes .= md5($this->randomState, true);
                } else {
                    $bytes .= pack('H*', md5($this->randomState));
                }
            }

            $bytes = substr($bytes, 0, $count);
        }

        return $bytes;
    }

    private function encodeBytes($input) {
        // The following is code from the PHP Password Hashing Framework
        $itoa64 = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        $output = '';
        $i = 0;
        do {
            $c1 = ord($input[$i++]);
            $output .= $itoa64[$c1 >> 2];
            $c1 = ($c1 & 0x03) << 4;
            if ($i >= 16) {
                $output .= $itoa64[$c1];
                break;
            }

            $c2 = ord($input[$i++]);
            $c1 |= $c2 >> 4;
            $output .= $itoa64[$c1];
            $c1 = ($c2 & 0x0f) << 2;

            $c2 = ord($input[$i++]);
            $c1 |= $c2 >> 6;
            $output .= $itoa64[$c1];
            $output .= $itoa64[$c2 & 0x3f];
        } while (1);

        return $output;
    }

}
